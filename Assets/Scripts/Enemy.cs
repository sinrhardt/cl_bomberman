using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;




public class Enemy : MonoBehaviour
{
    private enum Direction
    {
        Right,
        Left,
        Up,
        Down,
        Neutral
    }
    
    
    [SerializeField] private float velocity;
    
    private Rigidbody2D _rb;
    private Vector2 _velocityUpdated;
    private Direction _direction;
    
    public delegate void EnemyDeath();

    public static event EnemyDeath onEnemyDeath;

    
    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _velocityUpdated = new Vector2();
        _direction = Direction.Neutral;
        
        SetDirection();
        StartCoroutine(ChangeDirection());
    }


    private IEnumerator ChangeDirection()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);
            SetDirection();
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        _rb.position = RoundPos();
        SetDirection();    
    }

    private Vector2 RoundPos()
    {
        int x = Mathf.RoundToInt(_rb.position.x);
        int y = Mathf.RoundToInt(_rb.position.y);

        return new Vector2(x,y);
    }
    
    private void SetDirection()
    {
        float rand = Random.value;

        if (rand <= 0.25f && _direction != Direction.Right)
        {
            _velocityUpdated = Time.deltaTime * velocity * Vector2.right;
            _rb.velocity = _velocityUpdated;
            _direction = Direction.Right;
            return;
        }
        
        if (rand <= 0.50f && _direction != Direction.Down)
        {
            _velocityUpdated = Time.deltaTime * velocity * Vector2.down;
            _rb.velocity = _velocityUpdated;
            _direction = Direction.Down;
            return;
        }
        
        if (rand <= 0.75f && _direction != Direction.Left)
        {
            _velocityUpdated = Time.deltaTime * velocity * Vector2.left;
            _rb.velocity = _velocityUpdated;
            _direction = Direction.Left;
            return;
        }
        
        if (rand <= 1.00f && _direction != Direction.Up)
        {
            _velocityUpdated = Time.deltaTime * velocity * Vector2.up;
            _rb.velocity = _velocityUpdated;
            _direction = Direction.Up;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Bomb"))
        {
            gameObject.SetActive(false);
            onEnemyDeath?.Invoke();
        }
    }
}
