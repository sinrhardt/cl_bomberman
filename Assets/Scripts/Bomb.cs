using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] private GameObject explosionGameObject;
    

    
    private void OnEnable()
    {
        StartCoroutine(Explosion());
    }

    private IEnumerator Explosion()
    {
        yield return new WaitForSeconds(2);
        explosionGameObject.transform.position = transform.position;
        gameObject.SetActive(false);
        explosionGameObject.SetActive(true);
    }
    
}
