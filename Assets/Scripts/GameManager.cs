using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    private int _enemiesNumber;
    
    
    
    private void Awake()
    {
        Enemy.onEnemyDeath += SetEnemy;
    }

    private void Start()
    {
        _enemiesNumber = MapGenerator.Instance.EnemiesNumber;
        Debug.Log(_enemiesNumber);
    }

    private void OnDestroy()
    {
        Enemy.onEnemyDeath -= SetEnemy;
    }

    private void SetEnemy()
    {
        _enemiesNumber--;
        Debug.Log(_enemiesNumber);
        if (_enemiesNumber == 0)
        {
            SceneManager.LoadScene("WinScene");
        }
    }
    
}
