using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    private void OnEnable()
    {
        StartCoroutine(SelfDisable());
    }
    
    public delegate void Exploded();

    public static event Exploded onExploded;


    private IEnumerator SelfDisable()
    {
        yield return new WaitForSeconds(0.5f);
        gameObject.SetActive(false);
        onExploded?.Invoke();
    }
}
