using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MapGenerator : MonoBehaviour
{
    
    [Header("Adjustable values")]
    [SerializeField, Range(0.0f, 1.0f)] private float destructibleBlocksSpawnRate = 0.15f;
    [SerializeField] private int safeZone;
    [SerializeField] private int enemiesNumber;
    
    [Header("Reference")]
    [SerializeField] private GameObject mapRenderer;
    [SerializeField] private Transform MapFolder;
    [SerializeField] private Transform EnemiesFolder;
    [SerializeField] private GameObject indestructibleBlockPrefab;
    [SerializeField] private GameObject destructibleBlockPrefab;
    [SerializeField] private GameObject enemyPrefab;

    public static MapGenerator Instance { get; private set; }
    public int EnemiesNumber => enemiesNumber;
    private int _mapWidth;
    private int _mapHeight;
    private GameObject[,] _mapMatrix;

    private void Awake()
    {
        
        Instance = this;
    }

    void Start()
    {
        // Instantiate matrix
        _mapWidth = GetMapWidth();
        _mapHeight = GetMapHeight();
        _mapMatrix = new GameObject[_mapWidth, _mapHeight];
        InstantiateMatrix();
        
        // Generate blocks on the map
        GenerateMapBounds();
        GenerateIndBlocks();
        GenerateDesBlocks();
        
        // Generate enemies
        GenerateEnemies();
        
        
    }

    #region Map Generation

    private int GetMapWidth()
    {
        return (int) mapRenderer.transform.localScale.x;
    }

    private int GetMapHeight()
    {
        return (int) mapRenderer.transform.localScale.y;
    }

    private void InstantiateMatrix()
    {
        for (int i = 0; i < _mapWidth; i++)
        {
            for (int j = 0; j < _mapHeight; j++)
            {
                _mapMatrix[i, j] = new GameObject();
            }
        }
    }

    private void GenerateMapBounds()
    {
        // Generate first and last row
        for (int i = 0; i < _mapWidth; i++)
        {
            GameObject firstRowTemp = Instantiate(indestructibleBlockPrefab, MapFolder);
            GameObject lastRowTemp = Instantiate(indestructibleBlockPrefab, MapFolder);
            firstRowTemp.transform.position = new Vector3(i - _mapWidth/2,  _mapWidth/2, 0);            
            lastRowTemp.transform.position = new Vector3(i - _mapWidth/2, - _mapWidth/2, 0);

            _mapMatrix[i, 0] = firstRowTemp;
            _mapMatrix[i, _mapHeight - 1] = lastRowTemp;
        }

        for (int i = 1; i < _mapHeight - 1; i++)
        {
            GameObject leftColumnTemp = Instantiate(indestructibleBlockPrefab, MapFolder);
            GameObject rightColumnTemp = Instantiate(indestructibleBlockPrefab, MapFolder);
            leftColumnTemp.transform.position = new Vector3(- _mapWidth/2,i - _mapWidth/2, 0);            
            rightColumnTemp.transform.position = new Vector3( _mapWidth/2,i - _mapWidth/2, 0);
            
            _mapMatrix[0, i] = leftColumnTemp;
            _mapMatrix[_mapWidth - 1, i] = rightColumnTemp;
        }
    }

    private void GenerateIndBlocks()
    {
        for (int i = 1; i < _mapWidth - 1; i++)
        {
            for (int j = 1; j < _mapHeight - 1; j++)
            {
                if (i % 2 == 0 && j % 2 == 0)
                {
                    GameObject blockTemp = Instantiate(indestructibleBlockPrefab, MapFolder);
                    blockTemp.transform.position = new Vector3(i - _mapWidth / 2, j - _mapHeight / 2, 0);
                    _mapMatrix[i, j] = blockTemp;
                }
            }
        }
    }

    private void GenerateDesBlocks()
    {
        for (int i = 1; i < _mapWidth - 1; i++)
        {
            for (int j = 1; j < _mapHeight - 1; j++)
            {
                if ((i % 2 != 0 || j % 2 != 0) && Random.value <= destructibleBlocksSpawnRate && (i >= safeZone || j >= safeZone))
                {
                    GameObject blockTemp = Instantiate(destructibleBlockPrefab, MapFolder);
                    blockTemp.transform.position = new Vector3(i - _mapWidth / 2,_mapHeight / 2 - j, 0);
                    _mapMatrix[i, j] = blockTemp;
                }
            }
        }
    }

    private void GenerateEnemies()
    {
        for (int i = 0; i < enemiesNumber;)
        {
            int spawnX = Random.Range(0, _mapMatrix.GetLength(1));
            int spawnY = Random.Range(0, _mapMatrix.GetLength(0));

            
            if (! _mapMatrix[spawnX, spawnY].TryGetComponent<Block>(out Block block))
            {
                GameObject enemyTemp = Instantiate(enemyPrefab, EnemiesFolder);
                enemyTemp.transform.position = new Vector3(spawnX - _mapWidth / 2,_mapHeight / 2 - spawnY, 0);
                i++;
            }
        }
        
        
        
    }
    

    #endregion
    
    
}
