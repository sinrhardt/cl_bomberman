using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    [SerializeField] private float velocity = 100.0f;


    [SerializeField] private GameObject bombGameObject;
    
    private Rigidbody2D _rb;
    private bool _canDrop;



    private void Awake()
    {
        Explosion.onExploded += EnableDrop;
    }

    private void OnDestroy()
    {
        Explosion.onExploded -= EnableDrop;
    }

    void Start()
    {
        _canDrop = true;
        _rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        DropBomb();
    }

    public void FixedUpdate()
    {
        Movement();
    }

    #region Movement

    private void Movement()
    {
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float verticalInput = Input.GetAxisRaw("Vertical");

        Vector2 velocityUpdated = velocity * Time.deltaTime * new Vector2(horizontalInput, verticalInput);

        _rb.velocity = velocityUpdated;
    }
    

    void OnCollisionEnter2D(Collision2D collision)
    {

        _rb.velocity = Vector2.zero;

        if (collision.transform.CompareTag("Enemy"))
        {
            LoadLoseScene();
        }
    }
    
    #endregion

    #region Shooting

    private void DropBomb()
    {
        if (Input.GetKeyDown(KeyCode.Space) && _canDrop)
        {
            bombGameObject.transform.position = new Vector3(Mathf.RoundToInt(transform.position.x),Mathf.RoundToInt(transform.position.y), 0);
            bombGameObject.SetActive(true);
            _canDrop = false;
        }
    }

    public void EnableDrop()
    {
        _canDrop = true;
    }
    

    #endregion
    

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Bomb"))
        {
            LoadLoseScene();
        }
    }

    private void LoadLoseScene()
    {
        SceneManager.LoadScene("LoseScene");
    }
    
}
