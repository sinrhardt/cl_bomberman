using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BlockType
{
    IndestructibleBlock,
    DestructibleBlock
}

public class Block : MonoBehaviour
{
    [SerializeField] private BlockType _blockType;

    public BlockType BlockType => _blockType;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Bomb") && _blockType == BlockType.DestructibleBlock)
        {
            gameObject.SetActive(false);
        }
    }
}
